from django.conf.urls import patterns, include, url
from django.conf import settings as settings
from django.contrib import admin

from allauth.account import views as allauth_views
from allauth.socialaccount import views as socialaccount_views
from allauth.socialaccount.providers.facebook.views import oauth2_login as facebook_login
from allauth.socialaccount.providers.facebook.views import oauth2_callback as facebook_login_callback
 
#from dajaxice.core import dajaxice_autodiscover, dajaxice_config

from dajaxice.core import dajaxice_autodiscover

from stomory import settings as stomory_settings
from stomory.app.account.forms import CustomSignupForm, CustomResetPasswordForm, CustomSocialSignupForm
from stomory.app.app_settings import SIGNUP_TEMPLATE, SIGNUP_SUCCESS_URL, SOCIAL_SIGNUP_TEMPLATE
from stomory.app.app_settings import LOGIN_TEMPLATE, PASSWORD_RESET_TEMPLATE
from stomory.app.app_settings import PASSWORD_RESET_TEMPLATE_DONE
from stomory.app.account import views as stomory_account_views
from stomory.app.member.views import settings as profile_settings_view
from stomory.app.main import views as story_main_views
from stomory.settings import LOGOUT_REDIRECT_URL

dajaxice_autodiscover()
admin.autodiscover()

urlpatterns = patterns('',
    # Admin URL
    url(r'^admin/', include(admin.site.urls)),

    # Allauth app URL
    url(r"^signup/$",   stomory_account_views.signup, 
                        {
                            'template_name': SIGNUP_TEMPLATE,
                            'success_url': SIGNUP_SUCCESS_URL,
                            'form_class': CustomSignupForm 
                        }
                        , name="account_signup"),
    url(r"^login/$",    stomory_account_views.login,   
                        {
                            'template_name': LOGIN_TEMPLATE
                        }
                        , name="account_login"),
    url(r"^accounts/facebook/login/$", facebook_login, {}, name='facebook_login'),
    url(r"^accounts/facebook/login/callback/$", facebook_login_callback, {}, name='facebook_callback'),
    url(r"^accounts/social/signup/$",   socialaccount_views.signup,
                                        {
                                            "template_name": SOCIAL_SIGNUP_TEMPLATE,
                                            'form_class': CustomSocialSignupForm 
                                        }
                                        , name='socialaccount_signup'),

    url(r"^password/change/$", allauth_views.password_change, {'template_name': 'app/account/password_change.html'}, name="account_change_password"),
    url(r"^password/set/$", allauth_views.password_set, name="account_set_password"),
    url(r"^logout/$", allauth_views.logout, {
                                        'next_page': LOGOUT_REDIRECT_URL,
                                    }
                                    , name="account_logout"),
    url(r"^password/reset/$",   allauth_views.password_reset, 
                                {
                                    'template_name': PASSWORD_RESET_TEMPLATE,
                                    'form_class': CustomResetPasswordForm
                                }
                                , name="account_reset_password"),
    url(r"^password/reset/done/$",  allauth_views.password_reset_done, 
                                    {
                                        'template_name': PASSWORD_RESET_TEMPLATE_DONE
                                    }
                                    , name="account_reset_password_done"),
    url(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$", allauth_views.password_reset_from_key, name="account_reset_password_from_key"),

    # Custom Account URL
    url(r'^confirmation/resend/',stomory_account_views.resend_confirmation, name="account_resend_confirm_email"),
    url(r'^confirmation/(?P<confirmation_key>\w+)/',stomory_account_views.confirmation, name="account_confirm_email"),

    # stomory URL
    url(r'^$', include('stomory.app.main.urls')),
    url(r'^error/$', story_main_views.error_page),  
    url(r"^search/$",story_main_views.search_story),
    url(r"^filter/$",story_main_views.filter_story),
    url(r"^sort/$",story_main_views.sort_story),

    url(r'^story/', include('stomory.app.story.urls')),
    url(r'^settings/$', profile_settings_view),

    # Dajaxice URL
    url(r'^%s/' % stomory_settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),

    # Media URL
    url(r'^asset/media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    
    # Final URL - Member page
    url(r'^(?P<username>\w+)/',include('stomory.app.member.urls')),
)


