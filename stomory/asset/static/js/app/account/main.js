$(document).ready(function() {

	// Login by Facebook
	var button;
	window.fbAsyncInit = function() {
        FB.init({ appId: '495129700523326',
              	status: true, 
               	cookie: true,
               	xfbml: true,
               	oauth: true});
               
        function updateButton(response) {
            button = document.getElementById('facebook_auth');
                   
            button.onclick = function() {
            	console.log("in");
                FB.login(function(response) {
                    if (response.authResponse) {
                        FB.api('/me', function(info) {
                            FBlogin(response, info);
                        });    
                    } else {
	                  	console.log('fail');
                    }
                }, {scope:'email,user_birthday,status_update,friends_location,publish_stream,user_location,user_about_me,read_friendlists'});   
            }
      
        }
                
        // run once with current status and whenever the status changes
       	FB.getLoginStatus(updateButton);
        FB.Event.subscribe('auth.statusChange', updateButton);  
    };
    (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol 
                    + '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
    }());


})