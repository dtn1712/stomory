$(document).ready(function() {
	$('#handler_sort_dropdown').click(function(e) {
		$('.sort-filter-area .filter .filter-button').removeClass('dropdown-change');
		$('.sort-filter-area .filter .filter-button').css('border','1px solid transparent');
		if ($('.sort-filter-area .sort').hasClass('open')) {
			$('.sort-filter-area .sort .sort-button').removeClass('dropdown-change');
			$('.sort-filter-area .sort .sort-button').css('border','1px solid transparent');
		} else {
			$('.sort-filter-area .sort .sort-button').addClass('dropdown-change');
			$('.sort-filter-area .sort .sort-button').css('border','1px solid #D4D4D4');
		}
	});

	$('#handler_filter_dropdown').click(function(e) {
		$('.sort-filter-area .sort .sort-button').removeClass('dropdown-change');
		$('.sort-filter-area .sort .sort-button').css('border','1px solid transparent');
		if ($('.sort-filter-area .filter').hasClass('open')) {
			$('.sort-filter-area .filter .filter-button').removeClass('dropdown-change');
			$('.sort-filter-area .filter .filter-button').css('border','1px solid transparent');
		} else {
			$('.sort-filter-area .filter .filter-button').addClass('dropdown-change');
			$('.sort-filter-area .filter .filter-button').css('border','1px solid #D4D4D4');
		}
	});

	$(document).click(function(){
		$('.sort-filter-section .sort .sort-button').removeClass('dropdown-change');
		$('.sort-filter-area .sort .sort-button').css('border','1px solid transparent');
		$('.sort-filter-section .filter .filter-button').removeClass('dropdown-change');
		$('.sort-filter-area .filter .filter-button').css('border','1px solid transparent');
	});

})	