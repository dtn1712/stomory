// This file contain the initial script when running the project
$(document).ready(function() {
	$("#handler_create_story").click(function() {
		$('#choose_story_category_modal').modal('show')
	})

	$(".category-item .logo img").hover(
		function() {
			$(this).css("border","1px solid #f08712");
		},
		function() {
			$(this).css("border","1px solid #bababa");
		}
	);
	
	$('#story_container').masonry({
        itemSelector: '.story-item'
    });
})
