from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _, ugettext
from django.shortcuts import get_object_or_404

from stomory.app.app_settings import SESSION_KEY
from stomory.helper.common import get_user_login_object

from allauth.account import app_settings
from allauth.utils import email_address_exists


def check_user_login_page(request,username):
	if SESSION_KEY in request.session:
		user_id = request.session[SESSION_KEY]
		user_login = User.objects.get(pk=user_id)
		if user_login.username == username:
			return True
		else:
			return False
	else:
		return False

def validate_email_setting(request):
	value = request.POST['email']
	user_login = get_user_login_object(request)
	email = user_login.email
	print email
	if value != email:
		if value and email_address_exists(value):
			return -1
		else:
			return 1
	else:
		return 1