from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.utils import simplejson
from django.contrib.auth.models import User

from stomory.app.app_settings import SESSION_KEY, MESSAGE_SNIPPET_TEMPLATE
from stomory.app.account.models import StomoryEmailAddress

def remove_duplicate_object(l):
	return list(set(l))

def convert_queryset_to_list(obj_queryset):
	result = []
	if len(obj_queryset) != 0:
		for obj in obj_queryset:
			result.append(obj)
	return result

def get_user_login_object(request):
	if SESSION_KEY in request.session:
		user_id = request.session[SESSION_KEY]
		user = User.objects.filter(pk=user_id)
		if len(user) == 1:
			return user[0]
	return None

def generate_message(action,result,user_login):
	template_name = action + "_" + result
	snippet = render_to_string(MESSAGE_SNIPPET_TEMPLATE[template_name],{'user_login':user_login})
	return snippet

def handle_request_get_message(request):
	user_login = get_user_login_object(request)
	if "action" in request.GET and "result" in request.GET:	
		if user_login:
			return  generate_message(request.GET['action'],request.GET['result'],user_login)
		return None
	else:
		if user_login:
			email_confirmation = StomoryEmailAddress.objects.filter(email=user_login.email)
			if len(email_confirmation) != 0:
				print email_confirmation[0].verified
				if email_confirmation[0].verified == False: 
					return generate_message("confirm_email","asking",user_login)
		return None

