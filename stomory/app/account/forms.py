import base64
import re
import uuid
import datetime
import urllib2
import json

from django import forms
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.core import exceptions
from django.db.models import Q
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.http import int_to_base36
from django.utils.importlib import import_module

from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import Site
from django.shortcuts import get_object_or_404

from allauth.account.models import EmailAddress
from allauth.account.forms import BaseSignupForm
from allauth.account.utils import send_email_confirmation, format_email_subject
from allauth.account import app_settings
from allauth.utils import email_address_exists

from allauth.socialaccount.models import SocialAccount

from stomory.helper.account import set_readable_username
from stomory.app.account.models import StomoryEmailConfirmation, StomoryEmailAddress
from stomory.app.main.models import GENDER
from stomory.settings import WEBSITE_HOMEPAGE

class PasswordField(forms.CharField):

    def __init__(self, *args, **kwargs):
        render_value = kwargs.pop('render_value', 
                                  app_settings.PASSWORD_INPUT_RENDER_VALUE)
        kwargs['widget'] = forms.PasswordInput(render_value=render_value)
        super(PasswordField, self).__init__(*args, **kwargs)

class SetPasswordField(PasswordField):

    def clean(self, value):
        value = super(SetPasswordField, self).clean(value)
        min_length = app_settings.PASSWORD_MIN_LENGTH
        if len(value) < min_length:
            raise forms.ValidationError(_("Minimum {0} "
                                          "characters.").format(min_length))
        return value

class CustomSignupForm(BaseSignupForm):
    first_name = forms.CharField(
        label = _("Firstname"),
        max_length = 40,
        widget = forms.TextInput()
    )
    last_name = forms.CharField(
        label = _("Lastname"),
        max_length = 40,
        widget = forms.TextInput()
    )
    email = forms.EmailField(max_length=60,widget=forms.TextInput())
    password1 = SetPasswordField(label=_("Password"))
    password2 = PasswordField(label=_("Confirm Password"))
    gender = forms.CharField(label='Gender',required=False,max_length=10,widget=forms.Select(choices=GENDER))

    def clean_email(self):
        value = self.cleaned_data["email"]
        if app_settings.UNIQUE_EMAIL:
            if value and email_address_exists(value):
                raise forms.ValidationError \
                    (_("Unavailable Email Address"))
        return value

    def clean(self):
        super(CustomSignupForm, self).clean()
        if app_settings.SIGNUP_PASSWORD_VERIFICATION \
                and "password1" in self.cleaned_data \
                and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password each time."))
        return self.cleaned_data
    
    def create_user(self,request=None):
        email = self.cleaned_data['email']
        username = set_readable_username(email)
        first_name = self.cleaned_data["first_name"]
        last_name = self.cleaned_data["last_name"]
        gender = self.cleaned_data['gender']
        user = User.objects.create(username=username,email=email,first_name=first_name,last_name=last_name)
        password = self.cleaned_data.get("password1")
        if password:
            user.set_password(password)
        user.save()
        user.get_profile().gender = gender
        if gender == "1":
           user.get_profile().avatar = "default/img/user/male_icon.png"
        else:
           user.get_profile().avatar = "default/img/user/female_icon.png"
        user.get_profile().save()
        return user

    def send_activate_mail(self,user):
        email = user.email
        e = StomoryEmailConfirmation.objects.filter(email_address=email)
        is_sent = True
        if len(e) == 0:
            is_sent = False
        if is_sent:
            email_confirm = e[0]    
            email_confirm.send_confirmation()        
        else:
            new_email_confirm = StomoryEmailConfirmation.objects.create(email_address=email,sent=datetime.datetime.now())
            new_email_address = StomoryEmailAddress.objects.create(user=user,email=email)
            new_email_confirm.save()
            new_email_address.save()
            new_email_confirm.send_confirmation()

    def save(self,request=None):
        user = self.create_user(request)
        self.send_activate_mail(user)
        return user


class CustomResetPasswordForm(forms.Form):
    
    email = forms.EmailField(
        label = _("E-mail"),
        required = True,
        widget = forms.TextInput(attrs={"size":"30"})
    )
    
    def clean_email(self):
        email = self.cleaned_data["email"]
        self.users = User.objects.filter(Q(email__iexact=email)
                                         | Q(emailaddress__email__iexact=email)).distinct()
        if not self.users.exists():
            raise forms.ValidationError(_("This email is not available"))
        return self.cleaned_data["email"]
    
    def save(self, **kwargs):
        
        email = self.cleaned_data["email"]
        token_generator = kwargs.get("token_generator", default_token_generator)
        
        for user in self.users:
            
            temp_key = token_generator.make_token(user)
            
            # save it to the password reset model
            # password_reset = PasswordReset(user=user, temp_key=temp_key)
            # password_reset.save()
            
            current_site = Site.objects.get_current()

            # send the password reset email
            subject = format_email_subject(_("Password Reset E-mail"))
            path = reverse("account_reset_password_from_key",
                           kwargs=dict(uidb36=int_to_base36(user.id),
                                       key=temp_key))
            url = 'http://%s%s' % (WEBSITE_HOMEPAGE,
                                   path[1:])
            message = render_to_string \
                ("text/email/account/password_reset_key_message.html", 
                 { "site": current_site,
                   "user": user,
                   "password_reset_url": url })
            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email])
        return self.cleaned_data["email"]


class CustomResetPasswordKeyForm(forms.Form):
    
    password1 = SetPasswordField(label=_("New Password"))
    password2 = PasswordField(label=_("New Password (again)"))
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        self.temp_key = kwargs.pop("temp_key", None)
        super(ResetPasswordKeyForm, self).__init__(*args, **kwargs)
    
    # FIXME: Inspecting other fields -> should be put in def clean(self) ?
    def clean_password2(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password each time."))
        return self.cleaned_data["password2"]
    
    def save(self):
        # set the new user password
        user = self.user
        user.set_password(self.cleaned_data["password1"])
        user.save()
        # mark password reset object as reset
        # PasswordReset.objects.filter(temp_key=self.temp_key).update(reset=True)


class CustomSocialSignupForm(BaseSignupForm):
    def __init__(self, *args, **kwargs):
        self.sociallogin = kwargs.pop('sociallogin')
        user = self.sociallogin.account.user
        initial = { 'email': user.email or '',
                    'username': user.username or '',
                    'first_name': user.first_name or '',
                    'last_name': user.last_name or '' }
        kwargs['initial'] = initial
        super(CustomSocialSignupForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        value = self.cleaned_data["email"]
        if app_settings.UNIQUE_EMAIL:
            if value and email_address_exists(value):
                raise forms.ValidationError \
                    (_("Unavailable Email Address"))
        return value

    def save(self, request=None):
        new_user = self.create_user()
        socialaccount = self.sociallogin.account
        new_user.get_profile().avatar = "http://graph.facebook.com/" + socialaccount.uid + "/picture"
        link = "http://graph.facebook.com/" + socialaccount.uid
        data = urllib2.urlopen(link).read()
        json_data = json.loads(data)
        gender = json_data['gender']
        if gender == "male":
            new_user.get_profile().gender = "1"
        else:
            new_user.get_profile().gender = "0"
        new_user.get_profile().is_fb_account = True
        new_user.get_profile().save()
        self.sociallogin.account.user = new_user
        self.sociallogin.save()
        super(CustomSocialSignupForm, self).save(new_user) 
        # Confirmation last (save may alter first_name etc -- used in mail)
        send_email_confirmation(request, new_user)
        return new_user