# Create your views here.
from django import forms
from django.forms.util import ErrorList
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _, ugettext

from stomory.helper.common import get_user_login_object, handle_request_get_message
from stomory.helper.member import validate_email_setting, check_user_login_page
from stomory.app.main.models import StoryCategory, Story, Like


def main_view(request,story_id):
	user_login = get_user_login_object(request)
	message = handle_request_get_message(request)
	story_category = StoryCategory.objects.all()
	story = get_object_or_404(Story, pk=story_id)
	isFollow = False
	for follower in story.follow_by.all():
		if follower == user_login:
			isFollow = True
	isLike = bool(Like.objects.filter(story=story,user=user_login))
	print "like" + str(isLike)
	return render_to_response("app/story/page/main_view.html",
			{
				'message': message,
				'user_login': user_login,
				"story_category": story_category,
				'story': story,
				'isFollow': isFollow,
				'isLike': isLike
			},
			context_instance=RequestContext(request)
		)

def create_story(request):
	user_login = get_user_login_object(request)
	if request.method == "POST":
		title = request.POST['story_title']
		category = get_object_or_404(StoryCategory, pk=request.POST['category'])
		content = request.POST['elm1']
		privacy = request.POST['story_privacy']
		theme_picture = None
		if "theme_picture" in request.FILES:
			theme_picture = request.FILES['theme_picture']
		Story.objects.create(title=title,created_by=user_login,category=category,content=content,theme_picture=theme_picture)		
		return HttpResponseRedirect("/?action=create_story&result=success")
	if "category" in request.GET:
		category_id = request.GET['category']
		select_category = get_object_or_404(StoryCategory,pk=category_id)
		message = handle_request_get_message(request)
		story_category = StoryCategory.objects.all()
		return render_to_response("app/story/page/create.html",
							{
								"select_category": select_category,
								"story_category": story_category,
								"message": message,
								"user_login": user_login,
							},
							context_instance=RequestContext(request)
						)
	return HttpResponseRedirect("/")


def edit_story(request,story_id):
	user_login = get_user_login_object(request)
	story = get_object_or_404(Story, pk=story_id)
	message = handle_request_get_message(request)
	story_category = StoryCategory.objects.all()
	if request.method == "POST":
		title = request.POST['story_title']
		category = get_object_or_404(StoryCategory, pk=request.POST['category'])
		content = request.POST['elm1']
		privacy = request.POST['story_privacy']
		theme_picture = None
		if "theme_picture" in request.FILES:
			theme_picture = request.FILES['theme_picture']
		story.title = title
		story.category = category
		story.content = content
		story.theme_picture = theme_picture
		story.save()		
		return HttpResponseRedirect("/?action=edit_story&result=success")
	else:
		return render_to_response("app/story/page/edit.html",
							{
								"story_category": story_category,
								"message": message,
								"user_login": user_login,
								'story': story
							},
							context_instance=RequestContext(request)
						)

def delete_story(request,story_id):
	user_login = get_user_login_object(request)
	story = get_object_or_404(Story, pk=story_id)
	story.delete()
	return HttpResponseRedirect("/?action=delete_story&result=success")