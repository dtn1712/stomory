from django.utils import simplejson
from dajaxice.decorators import dajaxice_register
from stomory.helper.common import get_user_login_object
from stomory.app.main.models import Comment, Story, Like, Report

@dajaxice_register
def post_comment(request,comment_content,story_id):
    comment_by = get_user_login_object(request)
    comment = Comment(comment_by=comment_by,content=comment_content)
    comment.save()
    story = Story.objects.get(pk=story_id)
    story.comment.add(comment)
    return simplejson.dumps({})

@dajaxice_register
def delete_comment(request,comment_id):
	comment = Comment.objects.get(pk=comment_id)
	comment.delete()
	return simplejson.dumps({})

@dajaxice_register
def follow_story(request,story_id):
    user_login = get_user_login_object(request)
    story = Story.objects.get(pk=story_id)
    story.follow_by.add(user_login)
    return simplejson.dumps({})

@dajaxice_register
def unfollow_story(request,story_id):
	user_login = get_user_login_object(request)
	story = Story.objects.get(pk=story_id)
	story.follow_by.remove(user_login)
	return simplejson.dumps({})

@dajaxice_register
def like_story(request,story_id):
    user_login = get_user_login_object(request)
    story = Story.objects.get(pk=story_id)
    story.num_like += 1
    story.save()
    like = Like.objects.create(story=story,user=user_login)
    like.save()
    return simplejson.dumps({})

@dajaxice_register
def unlike_story(request,story_id):
	user_login = get_user_login_object(request)
	story = Story.objects.get(pk=story_id)
	story.num_like -= 1
	story.save()
	like = Like.objects.filter(story=story,user=user_login)
	if len(like) != 0:
		like[0].delete()
	return simplejson.dumps({})

@dajaxice_register
def report_story(request,story_id,reason):
    user_login = get_user_login_object(request)
    story = Story.objects.get(pk=story_id)
    report = Report.objects.create(report_by=user_login,report_story=story,reason=reason)
    report.save()
    return simplejson.dumps({})

