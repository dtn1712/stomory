from django.conf.urls.defaults import *
from stomory import settings

urlpatterns = patterns('stomory.app.story.views',
    url(r"^(?P<story_id>\d+)/$","main_view"),
    url(r"^create/$","create_story"),
    url(r"^edit/(?P<story_id>\d+)/$","edit_story"),
    url(r'^delete/(?P<story_id>\d+)/$',"delete_story")
)
