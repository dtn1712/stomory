from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User

from stomory.app.main.models import UserProfile, Story, Comment, Like, Report, StoryCategory

########################################
#                                      #
#      DEFINE CLASS ADMIN AREA         #
#                                      #
########################################
class UserProfileAdmin(admin.ModelAdmin):
	list_display = ['pk','user','gender','basic_info','is_fb_account']

class StoryAdmin(admin.ModelAdmin):
	list_display = ['pk','title','theme_picture','content','created_date',
					'num_like','privacy_status','category', 'created_by']

class ReportAdmin(admin.ModelAdmin):
    list_display = ['pk','report_story','report_by','date','reason']

class CommentAdmin(admin.ModelAdmin):
	list_display = ['pk','comment_by','content',
					'created_date']

class LikeAdmin(admin.ModelAdmin):
	list_display = ['pk','story','user']

class StoryCategoryAdmin(admin.ModelAdmin):
	list_display = ['pk','category_logo','name','description']
	

########################################
#                                      #
#     	     REGISTER AREA             #
#                                      #
########################################
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Story, StoryAdmin)
admin.site.register(Report, ReportAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Like, LikeAdmin)
admin.site.register(StoryCategory, StoryCategoryAdmin)


