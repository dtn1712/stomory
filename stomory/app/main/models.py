from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

import datetime
import uuid 

########################################
#                                      #
#           CONSTANT DATA              #
#                                      #
########################################
GENDER = (
      ("1","Male"),
      ('0',"Female")
    )

PRIVACY_STATUS = (
  ("1","Public"),
  ("0","Private"),
)

YES_NO = (
  ("1","Yes"),
  ("0","No"),
)

# User profile Model
class UserProfile(models.Model):
    # This field is required.
    user = models.ForeignKey(User, unique=True)

    gender = models.CharField(max_length=1,choices=GENDER)
    basic_info = models.TextField(blank=True)
    #avatar = models.ImageField("Profile Picture", upload_to="uploads/img/member/")
    #avatar = models.TextField(blank=True,null=True)
    is_fb_account = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.user)
    
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)

class Comment(models.Model):
  #comment_story = models.ForeignKey(Story,related_name='comment_story')
  comment_by = models.ForeignKey(User,related_name='comment_by')
  content = models.TextField()
  #content_html = models.TextField(editable=False, blank=True)
  created_date = models.DateTimeField(default=datetime.datetime.now())
  #created_elapse_time = models.CharField(max_length=50)
  #edit_date =  models.DateTimeField(blank=True,null=True)
  #edit_elapse_time = models.CharField(max_length=50,blank=True,null=True)
  class Meta:
    ordering = ['created_date']
      
  # def save(self, *args, **kwargs):
  #   now = datetime.datetime.now()
  #   created_elapse = now - self.created_date
  #   self.created_elapse_time = get_elapse_time(int(created_elapse.total_seconds())) 
  #   if self.edit_date != None:
  #       edit_elapse = now - self.edit_date
  #       self.edit_elapse_time = get_elapse_time(edit_elapse.total_seconds())
  #   self.content_html = markdown(self.content.replace("\n","<br>").replace(" ","&nbsp;"))
  #   super(Comment, self).save(*args, **kwargs)


class StoryCategory(models.Model):
  category_logo = models.ImageField("Category Logo", upload_to="default/img/category_story/")
  #category_logo = models.TextField(blank=True,null=True)	
  name = models.CharField(max_length=30)
  description = models.CharField(max_length=100)
  def __unicode__(self):
    return unicode(self.name)
    
class Story(models.Model):
  title = models.CharField(max_length=300)
  theme_picture = models.ImageField("Story Picture", upload_to="uploads/img/story/", blank=True, null=True) 
  #theme_picture = models.TextField(blank=True,null=True) 
  content = models.TextField()
  created_date = models.DateTimeField(default=datetime.datetime.now())
  privacy_status = models.CharField(max_length=1,choices=PRIVACY_STATUS,default='1')
  #social_publish = models.CharField(max_length=1,choices=YES_NO,default='1')
  category = models.ForeignKey(StoryCategory, related_name="category")
  created_by = models.ForeignKey(User, related_name="create_by")
  follow_by = models.ManyToManyField(User, related_name="follow_by",blank=True,null=True)
  comment = models.ManyToManyField(Comment,related_name='comment',blank=True,null=True)
  num_like = models.IntegerField(default=0)
  def __unicode__(self):
    return unicode(self.title)

class Report(models.Model):
  report_story = models.ForeignKey(Story,related_name='report_story')
  report_by = models.ForeignKey(User,related_name='report_by')
  date = models.DateTimeField(default=datetime.datetime.now())
  reason = models.TextField()
  class Meta:
    ordering = ['date']

class Like(models.Model):
  story = models.ForeignKey(Story,related_name='story')
  user = models.ForeignKey(User,related_name='user')

