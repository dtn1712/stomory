from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response

from stomory.helper.common import get_user_login_object, handle_request_get_message, convert_queryset_to_list

from stomory.app.main.models import StoryCategory, Story

def main_view(request):
	message = handle_request_get_message(request)
	user_login = get_user_login_object(request)
	story_category = StoryCategory.objects.all()
	list_story = Story.objects.all()[:20]
	return render_to_response(
			"app/main/page/index.html",
			{
				'message': message,
				'user_login': user_login,
				'list_story': list_story,
				'story_category': story_category
			},
			context_instance=RequestContext(request)
		)

def error_page(request):
	user_login = get_user_login_object(request)
	return render_to_response(
				"error.html",
				{
					"user_login": user_login,
				},
				context_instance=RequestContext(request)
			)

def search_story(request):
	query = request.GET['q']
	
	all_story = Story.objects.all()
	list_story = []
	for story in all_story:
		if (query.lower() in story.title.lower() or query.lower() in story.content.lower()) or story.category.name.lower() == query.lower():
			list_story.append(story)

	message = handle_request_get_message(request)
	user_login = get_user_login_object(request)
	story_category = StoryCategory.objects.all()
	return render_to_response(
			"app/main/page/index.html",
			{
				'message': message,
				'user_login': user_login,
				'list_story': list_story,
				'story_category': story_category
			},
			context_instance=RequestContext(request)
		)

def filter_story(request):
	filter_type = request.GET['type']
	list_story = []
	if filter_type != 'popular':
		all_story = Story.objects.all()
		for story in all_story:
			if story.category.name.lower() == filter_type.lower():
				list_story.append(story)
	else:
		list_story = Story.objects.all().order_by('-num_like')
	message = handle_request_get_message(request)
	user_login = get_user_login_object(request)
	story_category = StoryCategory.objects.all()
	return render_to_response(
			"app/main/page/index.html",
			{
				'message': message,
				'user_login': user_login,
				'list_story': list_story,
				'story_category': story_category
			},
			context_instance=RequestContext(request)
		
)
def sort_story(request):
	sort_type = request.GET['sortby']
	list_story = []
	if sort_type == 'latest':
		list_story = Story.objects.all().order_by('-created_date')
	elif sort_type == 'oldest':
		list_story = Story.objects.all().order_by('created_date')
	elif sort_type == 'ascending':
		list_story = Story.objects.all().order_by('title')
	elif sort_type == "descending":
		list_story = Story.objects.all().order_by('-title')
	message = handle_request_get_message(request)
	user_login = get_user_login_object(request)
	story_category = StoryCategory.objects.all()
	return render_to_response(
			"app/main/page/index.html",
			{
				'message': message,
				'user_login': user_login,
				'list_story': list_story,
				'story_category': story_category
			},
			context_instance=RequestContext(request)
		)
