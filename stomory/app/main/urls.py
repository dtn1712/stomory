from django.conf.urls.defaults import *
from stomory import settings

urlpatterns = patterns('stomory.app.main.views',
    url(r"^$", "main_view"),
)
