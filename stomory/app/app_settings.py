# Define all the settings for the swapleaf app here
SESSION_KEY = '_auth_user_id'

SIGNUP_TEMPLATE = "app/account/page/signup.html"
SIGNUP_SUCCESS_URL = "/?action=signup&result=success"
SOCIAL_SIGNUP_TEMPLATE = "app/account/page/social_signup.html"

LOGIN_TEMPLATE = "app/account/page/login.html"

PASSWORD_RESET_TEMPLATE = "app/account/page/password_reset.html"
PASSWORD_RESET_TEMPLATE_DONE = "app/account/page/password_reset_done.html"

MESSAGE_SNIPPET_TEMPLATE = 	{	
								"signup_success": "text/message/signup_success.html",
								"signup_error": "text/message/signup_error.html",
								"confirm_email_success": "text/message/confirm_email_success.html",
								"confirm_email_error": "text/message/confirm_email_error.html",
								"confirm_email_asking": "text/message/confirm_email_asking.html",
								"resend_confirm_email_success": "text/message/resend_confirm_email_success.html",
								"resend_confirm_email_error": "text/message/resend_confirm_email_error.html",
								"create_story_success": "text/message/create_story_success.html",
								"edit_story_success": "text/message/edit_story_success.html",
								"delete_story_success": "text/message/delete_story_success.html",
								"report_story_success": "text/message/report_story_success.html",
							}
