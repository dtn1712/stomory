# Create your views here.
from django import forms
from django.forms.util import ErrorList
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _, ugettext

from stomory.helper.common import get_user_login_object, handle_request_get_message
from stomory.helper.member import validate_email_setting, check_user_login_page

from stomory.app.member.form import SettingForm
def main_view(request,username):
	user_view = get_object_or_404(User,username=username)
	user_login = get_user_login_object(request)
	message = handle_request_get_message(request)

	template_name = ""
	if check_user_login_page(request,username):
		template = "app/member/page/view/login_user.html"
	else:
		template = "app/member/page/view/normal_user.html"

	return render_to_response(template,
			{
				'message': message,
				'user_view': user_view,
				'user_login': user_login,
			},
			context_instance=RequestContext(request)
		)

def settings(request):
	user_login = get_user_login_object(request)
	message = handle_request_get_message(request)
	if request.method =='POST':
		form = SettingForm(request.POST)

		return render_to_response("app/member/page/settings.html",
			{
				'form':form,
				'message': message,
				'user_login': user_login,
			},
			context_instance=RequestContext(request)
		)
	else:
		
		default_data = {
                        "first_name": user_login.first_name,
                        "last_name": user_login.last_name,
                        "email": user_login.email,
                       
                        }	
		form = SettingForm(default_data)
		return render_to_response("app/member/page/settings.html",
			{
				'form':form,
				'message': message,
				'user_login': user_login,
			},
			context_instance=RequestContext(request)
		)	